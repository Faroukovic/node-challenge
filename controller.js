var { saveChanges } = require('./ioHelper');

Array.prototype.add = function (element) {
    this.push(element);
    var res = saveChanges('dictionary.json',this);

}

Array.prototype.list = function (){
    this.forEach((element) => {
        console.log(`${element.key}: ${element.value}`);
    });
}

Array.prototype.get = function(key){
    var obj = this.find(ele => ele.key === key);
    return obj ? obj.value : null;
}

Array.prototype.remove = function(key){
    var arr = this.filter(ele => ele.key !== key);
    saveChanges('dictionary.json',arr);
}

Array.prototype.clear = function(){
    saveChanges('dictionary.json',[]);
}