var { loadData } = require('./ioHelper');
var proto = require('./controller');

var command = process.argv[2];
var key = process.argv[3];
var value = process.argv[4];

var dictionary = loadData('dictionary.json');

switch (command.toLowerCase()) {
    case 'add':
        dictionary.add({
            key: key,
            value: value
        });

        break;
    case 'list':
        console.log(`Data Count: ${dictionary.length}`);
        dictionary.list();
        break;
    case 'get':
        let val = dictionary.get(key);
        val ? console.log(val) : console.log(`Not Found`);
        break;
    case 'remove':
        dictionary.remove(key);
        break;
    case 'clear':
        dictionary.clear();
        // fs.writeFile("dictionary.json", JSON.stringify([]), (err) => {
        //     console.log(err);
        // });
        break;
    default:
        console.log("Undefined Command");
        break;
}