const fs = require('fs');

function loadData(filepath) {
    var file = fs.readFileSync(filepath);
    var dictionary = [];
    try {
        dictionary = JSON.parse(file)
    } catch (error) {
        dictionary = [];
    }
    return [].concat(dictionary);
}
function saveChanges(filepath, data) {
    try {
        fs.writeFileSync(filepath, JSON.stringify(data))
        console.log('Complete');
        
    } catch (error) {
        console.log("Somting Went Wrong Couldn't Save File");
    }
}
module.exports = {
    loadData,
    saveChanges
}